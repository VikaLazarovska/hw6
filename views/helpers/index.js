module.exports = (hbs) => {

    hbs.registerHelper("Phone", (e) => {
        let n = 0, num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        for (let i = 0; i < e.length; i++) {
            for (let a = 0; a < num.length; a++) {

                if (e[i] === num[a]) {
                    n++;
                }
            }
        }
        if (n > 10) {
            return new hbs.SafeString("<th class='greenNumber'>" + e + "</th>");
        } else {
            return new hbs.SafeString("<th>" + e + "</th>");
        }
    })

    hbs.registerHelper("Company", (e) => {
        if (e === "Microsoft") {
            return new hbs.SafeString("<th class='green'>" + e + "</th>");
        } else {
            return new hbs.SafeString("<th>" + e + "</th>");
        }
    })

    return hbs;
}